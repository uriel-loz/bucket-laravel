<?php
namespace App\Http\Controllers;
use App\Image;
use Aws\S3\S3Client;
use Aws\Exception\AwsException;
use Aws\Resource\Aws;
use App\Http\Requests\StoreImage;
use Illuminate\Support\Facades\Storage;

class ImageController extends Controller
{
    private $image;
    public function __construct(Image $image)
    {
        $this->image = $image;
    }
    public function getImages()
    {
        $images = [];

        $s3 = new S3Client( [
            'version' => 'latest',
            'region' => 'us-east-2',
            'credentials' => [
                'key' => env('AWS_ACCESS_KEY_ID',''),
                'secret' => env('AWS_SECRET_ACCESS_KEY',''),
            ]
        ] );

        $all_images = $this->image->all();

        foreach ( $all_images as $image ) {
            $result = $s3->getCommand( 'GetObject', [
                'Bucket' => env('AWS_BUCKET',''),
                'Key'    => $image->path
            ]);
    
            //The period of availability
            $request = $s3->createPresignedRequest($result, '+10 seconds');
    
            //Get the pre-signed URL
            $signedUrl = (string) $request->getUri();

            $images[] = [ 'url' => $signedUrl ];
        }

        return view( 'images', compact( 'images' ) );
      
    }
    public function postUpload(StoreImage $request)
    {
        $path = Storage::disk('s3')->put('images/originals', $request->file);
        $request->merge([
            'size' => $request->file->getClientSize(),
            'path' => $path
        ]);
        $this->image->create($request->only('path', 'title', 'size'));
        return back()->with('success', 'Image Successfully Saved');
    }
}